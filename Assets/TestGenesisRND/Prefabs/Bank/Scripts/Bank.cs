﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Bank
{
    public static int coins { get; private set; }
    private const string KEY = "BANK_KEY";

    public static event Action<int> OnValueChangedEvent = delegate { };

    public static void Initialize()
    {
        if (PlayerPrefs.HasKey(KEY))
            Load();
        else
        {
            coins = 0;
            Save();
        }

    }
    public static void AddCoin()
    {
        coins++;
        OnValueChangedEvent(coins);
        Save();
    }
    private static void Save()
    {
        PlayerPrefs.SetInt(KEY, coins);
        PlayerPrefs.Save();
    }
    private static void Load()
    {
        coins = PlayerPrefs.GetInt(KEY, 0);
    }
}
