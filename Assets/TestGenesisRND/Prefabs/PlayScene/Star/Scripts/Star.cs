﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    private const string PLAYER = "Player";
    private const string PLAYER_CLONE = "Player(Clone)";
    private const string FLOOR = "Floor";
    private float rotateSpeed = 180f;

    private void FixedUpdate()
    {
        TurnAround(rotateSpeed);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == PLAYER || other.gameObject.name == PLAYER_CLONE)
        {
            Bank.AddCoin();
            Destroy(this.gameObject);
        }
        if(other.gameObject.name == FLOOR)
        {
            Destroy(this.gameObject, 5f);
        }
    }
    private void TurnAround(float speedAround)
    {
        this.transform.Rotate(Vector3.up, speedAround * Time.fixedDeltaTime);
    }

}
