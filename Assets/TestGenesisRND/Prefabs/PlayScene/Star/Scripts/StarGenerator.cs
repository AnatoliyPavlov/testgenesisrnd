﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarGenerator : MonoBehaviour
{
    [SerializeField]
    private Transform startSpawnTransform;
    [SerializeField]
    private Transform endSpawnTransform;
    private const string FROM = "from";
    private const string TO = "to";
    private float timeCreating = 1f;
    private float timerCreating;
    [SerializeField]
    public GameObject star;
    // Start is called before the first frame update

    private void Awake()
    {
        startSpawnTransform = this.transform.Find(FROM);
        endSpawnTransform = this.transform.Find(TO);
        timerCreating= timeCreating;
        //star = FindObjectOfType<Star>().gameObject;/* Resources.Load<GameObject>(STAR);*/
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timerCreating > 0)
            timerCreating -= Time.deltaTime;
        else
        {
            float x = Random.Range(startSpawnTransform.position.x, endSpawnTransform.position.x);
            float y = this.transform.position.y;
            float z = Random.Range(startSpawnTransform.position.z, endSpawnTransform.position.z);
            Vector3 createPosition = new Vector3(x,y,z);
            Quaternion quat = new Quaternion();
            Instantiate(star, createPosition, quat, this.transform);
            timerCreating = timeCreating;
        }
    }
}
