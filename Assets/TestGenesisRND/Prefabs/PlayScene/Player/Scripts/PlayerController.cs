﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private Vector3 movingPosition;
    void Awake()
    {
        if (speed==0f)
            speed = 5f;
    }
    private void Start()
    {
        ChangeMovingPosition();
    }
    void Update()
    {

    }
    private void FixedUpdate()
    {
        if (this.transform.position == movingPosition)
            ChangeMovingPosition();
        MoveToTarget(movingPosition);
        LookToTarget(movingPosition);
    }
    private void ChangeMovingPosition()
    {
        movingPosition = new Vector3(Random.Range(-4.7f, 4.7f), 0, this.transform.position.z + 4);
    }
    private void MoveToTarget(Vector3 target)
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, target, this.speed * Time.fixedDeltaTime);
    }

    private void LookToTarget(Vector3 lookPosition)
    {
        this.transform.LookAt(lookPosition);
    }
}
