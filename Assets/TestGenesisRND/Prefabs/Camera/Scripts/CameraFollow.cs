﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private Transform playerTransform;
    [SerializeField]
    private Vector3 startPosition;
    void Awake()
    {
        playerTransform = FindObjectOfType<PlayerController>().GetComponent<Transform>();
        startPosition = this.gameObject.transform.position- playerTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        this.transform.position = playerTransform.position + startPosition;
    }
}
