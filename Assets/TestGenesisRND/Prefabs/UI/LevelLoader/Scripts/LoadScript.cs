﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

public class LoadScript : MonoBehaviour
{
    [SerializeField]
    private WWWLoadData wwwLoadData;
    private bool isInternet;

    public bool isLoad { get; private set; }

    private class WWWLoadData
    {
        public string bundleURL;
        public string bundleName;
        public string manifestURL;
        public string bundleHash128;
    }

    private void Awake()
    {
        wwwLoadData = new WWWLoadData();
        isInternet = true;
    }
    IEnumerator Start()
    {
        isLoad = false;

        while (!Caching.ready)
            yield return null;

        if (File.Exists($"{Application.dataPath}/assetBundleSettings.json"))
        {
            LoadFromJson();
        }
        else
        {
            throw new System.Exception("json not found");
        }

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            isInternet = false;
            Debug.Log("No internet");
        }

        if (isInternet)
        {
            Debug.Log("Try to load Manifest");
            using (WWW www = new WWW(wwwLoadData.manifestURL))
            {
                yield return www;

                if (!string.IsNullOrEmpty(www.error))
                {
                    Debug.Log(www.error);
                    yield break;
                }

                Debug.Log("Loading Manifest");

                AssetBundle assetBundle = www.assetBundle;
                yield return assetBundle;

                AssetBundleManifest assetBundleManifest = assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                yield return assetBundleManifest;
                Debug.Log("Manifest is load");

                Hash128 assetBundleHash128 = assetBundleManifest.GetAssetBundleHash($"{wwwLoadData.bundleName}");

                //Сравнение хэшей
                if (wwwLoadData.bundleHash128 != assetBundleHash128.ToString())
                {
                    this.wwwLoadData.bundleHash128 = assetBundleHash128.ToString();
                    SaveToJson();
                }
                yield return null;

                assetBundle.Unload(false);
            }
        }
        if (!string.IsNullOrEmpty(wwwLoadData.bundleHash128))
        {
            Debug.Log("Try to load Data");
            using (WWW www = WWW.LoadFromCacheOrDownload(wwwLoadData.bundleURL, Hash128.Parse(wwwLoadData.bundleHash128)))
            {
                yield return www;

                if (!string.IsNullOrEmpty(www.error))
                {
                    Debug.Log(www.error);
                    yield break;
                }

                Debug.Log("Loading Data");

                AssetBundle assetBundle = www.assetBundle;
                yield return assetBundle;
                Debug.Log("Data is load");

                if (!isInternet)
                {
                    FindObjectOfType<UIController>().ShowOfflineMod();
                }
                InstantiateObjectOnSceneFromAssetBundle(assetBundle);
                assetBundle.Unload(false);
                yield return null;
                this.isLoad = true;
            }
        }
        else
        {
            FindObjectOfType<UIController>().ShowNoInternet();
            yield return null;
        }
    }

    private void LoadFromJson()
    {
        string json = File.ReadAllText($"{Application.dataPath}/assetBundleSettings.json");
        this.wwwLoadData = JsonUtility.FromJson<WWWLoadData>(json);
    }
    private void SaveToJson()
    {
        Debug.Log("SaveToJson");
        string json = JsonUtility.ToJson(this.wwwLoadData);
        File.WriteAllText($"{Application.dataPath}/assetBundleSettings.json", json);
    }
    private AssetBundle InstantiateObjectOnSceneFromAssetBundle(AssetBundle assetBundle)
    {
        string assetName = "Directional Light";
        Instantiate(assetBundle.LoadAsset(assetName));
        assetName = "GameField";
        Instantiate(assetBundle.LoadAsset(assetName));
        assetName = "Player";
        Instantiate(assetBundle.LoadAsset(assetName));
        assetName = "MainCamera";
        Instantiate(assetBundle.LoadAsset(assetName));
        assetName = "Star";
        FindObjectOfType<StarGenerator>().star = (GameObject)assetBundle.LoadAsset(assetName);
        return assetBundle;
    }
}
