﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private Animator uiAnimator;
    [SerializeField]
    private Button settingsButton;
    [SerializeField]
    private Button settingsBackButton;
    [SerializeField]
    private Button playButton;
    [SerializeField]
    private Button menuButton;
    [SerializeField]
    private AudioSource buttonClickSFX;
    [SerializeField]
    private AudioSource startMenuMusic;
    [SerializeField]
    private AudioSource playMusic;
    [SerializeField]
    private Toggle sfxToggle;
    [SerializeField]
    private Toggle musicToggle;
    [SerializeField]
    private AudioMixer audioMixer;
    [SerializeField]
    private CanvasGroup canvasGroup;
    [SerializeField]
    private GameObject menu;
    [SerializeField]
    private GameObject loadScreen;
    [SerializeField]
    private GameObject background;
    [SerializeField]
    private GameObject needInternetMessage;
    [SerializeField]
    private GameObject offlineModeMessage;
    [SerializeField]
    private Text coinsValueText;


    private void Awake()
    {
        if (!audioMixer)
            audioMixer = Resources.Load<AudioMixer>("AudioMixer");

        Bank.Initialize();
        coinsValueText.text = $"{Bank.coins}";
        
    }
    private void Start()
    {
        AddListeners();
    }

    private void AddListeners()
    {
        if (settingsButton)
            SettingsButtonOnClickAddListener();
        if (settingsBackButton)
            SettingsBackButtonOnClickAddListener();
        if (playButton)
            PlayButtonOnClickAddListener();
        if (menuButton)
            MenuButtonOnClickAddListener();
        if (sfxToggle)
            SFXToogleOnValueChangedAddListener();
        if (musicToggle)
            MusicToogleOnValueChangedAddListener();
        Bank.OnValueChangedEvent += ChangeCoinsValueText;
    }

    private void SettingsButtonOnClickAddListener()
    {
        settingsButton.onClick.AddListener(MenuSwipeLeftAnimationPlay);
        settingsButton.onClick.AddListener(ButtonClickSoundPlay);
    }

    private void SettingsBackButtonOnClickAddListener()
    {
        settingsBackButton.onClick.AddListener(MenuSwipeRightAnimationPlay);
        settingsBackButton.onClick.AddListener(ButtonClickSoundPlay);
    }

    private void PlayButtonOnClickAddListener()
    {
        playButton.onClick.AddListener(LoadingPlayScene);
        playButton.onClick.AddListener(ButtonClickSoundPlay);
    }
    private void MenuButtonOnClickAddListener()
    {
        menuButton.onClick.AddListener(LoadingStartScene);
        menuButton.onClick.AddListener(ButtonClickSoundPlay);
    }

    private void SFXToogleOnValueChangedAddListener()
    {
        sfxToggle.onValueChanged.AddListener((value) => ChangeSFXVolume(value));

    }

    private void MusicToogleOnValueChangedAddListener()
    {
        musicToggle.onValueChanged.AddListener((value) => ChangeMusicVolume(value));
    }

    private void MenuSwipeLeftAnimationPlay()
    {
        uiAnimator.SetTrigger("SwipeLeft");
        AllButtonsDisable();
    }

    private void MenuSwipeRightAnimationPlay()
    {
        uiAnimator.SetTrigger("SwipeRight");
        AllButtonsDisable();
    }

    private void LoadingPlayScene()
    {
        StartCoroutine(FadeCoroutine(true));
    }

    private void LoadingStartScene()
    {
        StartCoroutine(FadeCoroutine(false));
    }

    private void ButtonClickSoundPlay()
    {
        if(buttonClickSFX)
        buttonClickSFX.Play();
    }

    private void ChangeSFXVolume(bool isOn)
    {
        if(isOn)
            audioMixer.SetFloat("SFXVolume", 0);
        else
            audioMixer.SetFloat("SFXVolume", -80);
    }

    private void ChangeMusicVolume(bool isOn)
    {
        if (isOn)
            audioMixer.SetFloat("MusicVolume", 0);
        else
            audioMixer.SetFloat("MusicVolume", -80);

    }

    public void EndAnimation()
    {
        AllButtonsEnable();
    }

    public void ShowNoInternet()
    {
             needInternetMessage.SetActive(true);
    }
    public void ShowOfflineMod()
    {
        offlineModeMessage.SetActive(true);
    }

    private void AllButtonsDisable()
    {
        settingsButton.interactable = false;
        settingsBackButton.interactable = false;
        playButton.interactable = false;
        sfxToggle.interactable = false;
        musicToggle.interactable = false;
    }

    private void AllButtonsEnable()
    {
        settingsButton.interactable = true;
        settingsBackButton.interactable = true;
        playButton.interactable = true;
        sfxToggle.interactable = true;
        musicToggle.interactable = true;
    }

    private IEnumerator FadeCoroutine(bool isPlayScene)
    {
        AllButtonsDisable();

        for (float i = 1f; i > 0; i -= Time.deltaTime)
        {
            canvasGroup.alpha = i;
            yield return null;
        }
        menu.SetActive(false);
        background.SetActive(false);
        loadScreen.SetActive(true);
        for (float i = 0f; i < 1; i += Time.deltaTime)
        {
            canvasGroup.alpha = i;
            yield return null;
        }
        if (isPlayScene)
            LoadNextScene();
        else
            LoadStartScene();
        yield return null;

        if (isPlayScene)
        {
            //Ожидание загрузки объектов с сайта
            var loadScript = FindObjectOfType<LoadScript>();
            while (!loadScript.isLoad)
            {
                yield return null;
            }
            //=======
            startMenuMusic.Stop();
            playMusic.Play();
        }
        else
        {
            startMenuMusic.Play();
            playMusic.Stop();
            AllButtonsEnable();
        }

        for (float i = 1f; i > 0; i -= Time.deltaTime)
        {
            canvasGroup.alpha = i;
            yield return null;
        }
        loadScreen.SetActive(false);
        menu.SetActive(true);
        if (isPlayScene)
        {
            menu.transform.Find("StartMenu").gameObject.SetActive(false);
            menu.transform.Find("SettingsMenu").gameObject.SetActive(false);
            menu.transform.Find("PlayMenu").gameObject.SetActive(true);
        }
        else
        {
            background.SetActive(true);
            menu.transform.Find("StartMenu").gameObject.SetActive(true);
            menu.transform.Find("SettingsMenu").gameObject.SetActive(true);
            menu.transform.Find("PlayMenu").gameObject.SetActive(false);
        }
        for (float i = 0f; i < 1; i += Time.deltaTime)
        {
            canvasGroup.alpha = i;
            yield return null;
        }
    }

    private void LoadNextScene()
    {
        SceneLoader.LoadNextScene();
    }

    private void LoadStartScene()
    {
        SceneLoader.LoadStartScene();
    }

    private void ChangeCoinsValueText(int value)
    {
        coinsValueText.text = $"{value}";
    }
}
