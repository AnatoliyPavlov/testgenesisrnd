﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneLoader
{
    private static void LoadScene(int buildIndex)
    {
        if (buildIndex < SceneManager.sceneCountInBuildSettings)
            SceneManager.LoadScene(buildIndex);
    }

    public static void LoadNextScene()
    {
        var buildIndexNext = SceneManager.GetActiveScene().buildIndex + 1;
        if (buildIndexNext < SceneManager.sceneCountInBuildSettings)
            SceneManager.LoadScene(buildIndexNext);
    }

    public static void RestarScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public static void LoadStartScene()
    {
        SceneManager.LoadScene(0);
    }
}
