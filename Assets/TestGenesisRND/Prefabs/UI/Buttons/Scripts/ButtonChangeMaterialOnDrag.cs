﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonChangeMaterialOnDrag : MonoBehaviour
{
    [SerializeField]
    private Image thisImage;
    [SerializeField]
    private Material buttonMaterial;
    [SerializeField]
    private Material buttonOnDragMaterial;

    void Awake()
    {
        thisImage = gameObject.GetComponent<Image>();
        buttonMaterial = Resources.Load<Material>("ButtonMaterial");
        buttonOnDragMaterial = Resources.Load<Material>("ButtonClickMaterial");
    }

    void OnMouseDrag()
    {
        thisImage.material = buttonOnDragMaterial;
    }

    void OnMouseUp()
    {
        thisImage.material = buttonMaterial;
    }
}
