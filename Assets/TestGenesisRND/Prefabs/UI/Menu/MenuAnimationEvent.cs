﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAnimationEvent : MonoBehaviour
{
    private void EndAnimation()
    {
        FindObjectOfType<UIController>().EndAnimation();
    }
}
