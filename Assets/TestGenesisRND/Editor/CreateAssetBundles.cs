﻿using UnityEditor;

public class CreateAssetBundles : Editor
{
    [MenuItem("Assets/Build AssetBundles")]
    static void BuiladAllAssetBundles()
    {
        BuildPipeline.BuildAssetBundles("Assets/TestGenesisRND/AssetBundles/", BuildAssetBundleOptions.None, BuildTarget.Android);
    }
}